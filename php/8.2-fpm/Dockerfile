FROM php:8.2-fpm

ARG TIMEZONE=Europe/Berlin

RUN unlink /etc/localtime && ln -s /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata
RUN echo "date.timezone=`cat /etc/timezone`" > /usr/local/etc/php/conf.d/timezone.ini

RUN apt-get -qq update \
 && apt-get -qq install --no-install-recommends --no-install-suggests -y \
    vim less wget curl procps openssh-client rsync locales \
    zip unzip git \
    libgd3 libgd-dev freetype* libjpeg-dev libpng-dev libwebp-dev libxpm-dev libzip-dev libbz2-dev \
    libmcrypt-dev \
    zlib1g-dev libicu-dev \
    libxml2-dev \
 && apt-get autoremove -y \
 && apt-get clean -y

RUN docker-php-ext-install bcmath
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd
RUN docker-php-ext-install iconv
RUN docker-php-ext-install intl
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install opcache
RUN docker-php-ext-install soap
RUN docker-php-ext-install zip
RUN docker-php-ext-install bz2

# https://pecl.php.net/package/mcrypt
RUN yes '' | pecl install mcrypt-1.0.7 \
 && echo extension="mcrypt.so" > /usr/local/etc/php/conf.d/php-ext-mcrypt.ini


RUN wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php'); unlink('installer.sig');"
RUN mv composer.phar /usr/local/bin/composer

RUN php -m
