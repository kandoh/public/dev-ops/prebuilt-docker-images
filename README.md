# prebuild-docker-images
This repository will build docker images with pre-built dependencies/libraries and push them to Docker Hub.

## [`kandoh/php`][kandoh/php]
* `kandoh/php:7.4-cli`
* `kandoh/php:7.4-fpm`
* `kandoh/php:8.0-cli`
* `kandoh/php:8.0-fpm`
* `kandoh/php:8.1-cli`
* `kandoh/php:8.1-fpm`
* `kandoh/php:8.2-cli`
* `kandoh/php:8.2-fpm`
* `kandoh/php:8.3-cli`
* `kandoh/php:8.3-fpm`
* `kandoh/php:8.4-cli`
* `kandoh/php:8.4-fpm`

## [`kandoh/node`][kandoh/node]
* `kandoh/node:14`, with `dart-sass` and `node-sass@5`
* `kandoh/node:16`, with `dart-sass` and `node-sass@6`
* `kandoh/node:18`, with `dart-sass` and `node-sass@8`
* `kandoh/node:20`, with `dart-sass` and `node-sass@9`
* `kandoh/node:21`, with `dart-sass` and `node-sass@9`


---
[kandoh/php]: https://hub.docker.com/r/kandoh/php
[kandoh/node]: https://hub.docker.com/r/kandoh/node
